#!/bin/bash

set -ex

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OUTPUT_DIR=$PWD/public
TEMPLATE_DIR=$PWD/html
ARTIFACTS="debian*rootfs.tar.gz"

mkdir -p ${OUTPUT_DIR}

cp ${TEMPLATE_DIR}/style.css ${OUTPUT_DIR}/
envsubst <${TEMPLATE_DIR}/header.html.templ >${OUTPUT_DIR}/index.html

pushd $OUTPUT_DIR
for a in $ARTIFACTS; do
    export FILE_NAME=$a
    export CHECKSUM="sha256"
    export FILE_CHECKSUM="$a.$CHECKSUM.txt"
    export FILE_DATE="$(date "+%Y-%m-%d %H:%M UTC%:z")" 
    envsubst <"$TEMPLATE_DIR/tr.html.templ" >>${OUTPUT_DIR}/index.html
    sha256sum $a |cut -d\  -f1 >${OUTPUT_DIR}/${FILE_CHECKSUM}
done
popd

envsubst <$TEMPLATE_DIR/footer.html.templ >>${OUTPUT_DIR}/index.html
