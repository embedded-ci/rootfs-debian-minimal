#!/bin/bash

set -ex

SUITE=${SUITE:-stretch}
ARCH=${ARCH:-arm64}
LOCAL_BUILDDIR="${LOCAL_BUILDDIR:-$PWD}"
ROOTFS_DIR="$LOCAL_BUILDDIR/rootfs"
TAR_FILE="$LOCAL_BUILDDIR/debian_${SUITE}_${ARCH}_rootfs.tar.gz"
ARTIFACTS_DIR="${ARTIFACTS_DIR:-$PWD/public}"

OUTPUT="${TAR_FILE}"

MIRROR_DEBIAN=http://ftp.us.debian.org/debian
MIRROR_RASPBIAN=http://archive.raspbian.org/raspbian

QEMU_ARCH=""
case $ARCH in
    'amd64')
	    export MIRROR=${MIRROR_DEBIAN}
        ;;
    'arm64')
        export QEMU_ARCH=aarch64
	    export MIRROR=${MIRROR_DEBIAN}
        ;;
    'armhf')
        export QEMU_ARCH=arm
	    export MIRROR=${MIRROR_DEBIAN}
        ;;
    'armv6-vfp2')
        export QEMU_ARCH=arm
	    export MIRROR=${MIRROR_RASPBIAN}
        # TAR_FILE has armv6-vfp2 in at this point, debootrap needs ARCH=armhf even for armv6-vfp2
        export ARCH=armhf
        # We need to verify agains the the raspbian public key
        KEYRING="--keyring=/etc/apt/trusted.gpg"
        ;;
    *)
        echo "ERROR: unknwon ARCH=$ARCH"
        exit 1
        ;;
    armv6-vfp2)
        QEMU_ARCH=arm
	MIRROR=${MIRROR_RASPBIAN}
esac

echo "###################################"
echo "#### Running debootstrap stage 1 ##"
echo "###################################"
[[ -d "${ROOTFS_DIR}" ]] && rm -rf "${ROOTFS_DIR}"
mkdir -p "${ROOTFS_DIR}"
ls -la "${ROOTFS_DIR}"
id -u
id -g

if [[ -z "$QEMU_ARCH" ]]; then
    /usr/sbin/debootstrap --verbose --arch ${ARCH} ${SUITE} "${ROOTFS_DIR}" ${MIRROR}
    echo "OK"
else
    CMD="/usr/sbin/debootstrap ${KEYRING} ${KEYRING} --verbose --foreign --arch ${ARCH} ${SUITE} ${ROOTFS_DIR} ${MIRROR}"
    echo $CMD
    $CMD
    echo "OK"
    echo -n "adding qemu-$QEMU_ARCH-static..."
    cp -va "$(which qemu-$QEMU_ARCH-static)" "${ROOTFS_DIR}"/usr/bin/
    echo "OK"

    update-binfmts --display qemu-$QEMU_ARCH
    update-binfmts --enable qemu-$QEMU_ARCH

    echo "##########################"
    echo "## ENTER CHROOT         ##"
    echo "##########################"
    
    LLANG=C.UTF-8 chroot "${ROOTFS_DIR}" /bin/bash <<EOF
echo -n "Running debootstrap stage 2..."
/debootstrap/debootstrap --second-stage || cat /debootstrap/debootstrap.log
echo "OK"
EOF
    
    echo "##########################"
    echo "## LEAVE CHROOT         ##"
    echo "##########################"
fi

pushd ${ROOTFS_DIR}
tar --one-file-system -cvzf "${TAR_FILE}" .
popd

mkdir -p public
mv debian*rootfs.tar.gz "${ARTIFACTS_DIR}"/
